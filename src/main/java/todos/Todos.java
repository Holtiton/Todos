package todos;

import javafx.application.Application;
import todos.GUI.TodosGUI;

class Todos {
    public static void main(String[] args) {
        Application.launch(TodosGUI.class, args);
    }
}