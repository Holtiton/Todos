package todos.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Callback;
import todos.GUI.controllers.ControlsController;
import todos.GUI.controllers.MenuController;
import todos.GUI.controllers.OverviewController;
import todos.GUI.controllers.StatusController;
import todos.repository.InMemoryTodoRepository;
import todos.repository.ObservableTodoRepository;
import todos.repository.ObservableTodoRepositoryWrapper;
import todos.repository.TodoRepository;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class TodosGUI extends Application {
    private TodoRepository base = new InMemoryTodoRepository();
    private ObservableTodoRepository repo = new ObservableTodoRepositoryWrapper(base);

    /**
     * Map for injection of <code>{@link ObservableTodoRepositoryWrapper}</code> to controller constructors
     * <p>
     * FXMLLoader with a custom controller factory expects a <code>Callabel</code> instance with
     * the controller. This functions create a map of the controller classes and how
     * to call them. Normally FXMLLoader calls a no-arg constructor for classes but with
     * this the creation of the controllers can be changed. In this case I want a repository
     * reference to each of the controller classes.
     * <p>
     * Only the FMXL files instanced by main.fxml needs these mappings. Additional windows
     * instantiated elsewhere need their own mappings or direct access to the controller.
     * TODO: maybe not every controller needs it?
     */
    private Map<Class, Callable<?>> createMappings() {
        Map<Class, Callable<?>> mappings = new HashMap<>();
        mappings.put(ControlsController.class, (Callable<ControlsController>) () -> new ControlsController(repo));
        mappings.put(MenuController.class, (Callable<MenuController>) () -> new MenuController(repo));
        mappings.put(OverviewController.class, (Callable<OverviewController>) () -> new OverviewController(repo));
        mappings.put(StatusController.class, (Callable<StatusController>) () -> new StatusController(repo));
        return mappings;
    }

    /**
     * Callback for FXMLLoaders setControllerFactory
     * <p>
     * This function creates the Callback function that <code>{@link FXMLLoader#setControllerFactory(Callback)}</code>
     * expects. This is used as a DIY DI as this is a small app and I didn't feel like grabbing a dependency to a DI
     * framework. See <code>{@link TodosGUI#createMappings()}</code> for the more details.
     */
    private Callback<Class<?>, Object> createControllerFactory() {
        Map<Class, Callable<?>> mappings = createMappings();
        return param -> {
            Callable<?> callable = mappings.get(param);
            if (callable == null) {
                try {
                    // default handling: use no-arg constructor
                    return param.getDeclaredConstructor().newInstance();
                } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException ex) {
                    throw new IllegalStateException(ex);
                }
            } else {
                try {
                    return callable.call();
                } catch (Exception ex) {
                    throw new IllegalStateException(ex);
                }
            }
        };
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Todos");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/Main.fxml"));
        loader.setControllerFactory(createControllerFactory());
        primaryStage.setScene(new Scene(loader.load()));
        primaryStage.show();
    }
}
