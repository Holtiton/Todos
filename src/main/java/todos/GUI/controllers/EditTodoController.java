package todos.GUI.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import todos.model.TodoItem;
import todos.repository.ObservableTodoRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EditTodoController {
    private ObservableTodoRepository repo;
    private TodoItem todoItem = null;

    public void setRepo(ObservableTodoRepository repo) {
        this.repo = repo;
    }

    public void setTodo(TodoItem todo) {
        this.todoItem = todo;
    }

    @FXML
    TextField title;

    @FXML
    TextArea details;

    @FXML
    DatePicker scheduled;

    @FXML
    TextField project;

    @FXML
    TextField context;

    @FXML
    Label created;

    @FXML
    Hyperlink unschedule;

    public void initialize() {

        scheduled.setConverter(new StringConverter<LocalDate>() {
            private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

            @Override
            public String toString(LocalDate localDate) {
                if(localDate == null) {
                    return "";
                }
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString) {
                if(dateString == null || dateString.trim().isEmpty()) {
                    return null;
                }
                return LocalDate.parse(dateString,dateTimeFormatter);
            }
        });
        scheduled.setDayCellFactory(picker -> new DateCell() {
            final LocalDate today = LocalDate.now();
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                setDisable(empty || date.isBefore(today));
            }
        });
        scheduled.setEditable(false);

        unschedule.setOnMouseClicked(event -> scheduled.setValue(null));
        unschedule.disableProperty().bind(scheduled.valueProperty().isNull());
        unschedule.visitedProperty().bind(scheduled.valueProperty().isNull());

        Platform.runLater(this::populateFields);
    }

    private void populateFields() {
        if (todoItem != null) {
            title.setText(todoItem.getTitle());
            details.setText(todoItem.getDetails());
            scheduled.setValue(todoItem.getScheduledDate());
            project.setText(String.join(", ", todoItem.getProjects()));
            context.setText(String.join(", ", todoItem.getContexts()));
            created.setText(todoItem.getCreatedDate().toString());
        } else {
            created.setText(LocalDate.now().toString());
        }
    }

    public TodoItem getTodoItem() {
        if (todoItem != null) {
            todoItem.setTitle(title.getText());
            todoItem.setDetails(details.getText());
            if (scheduled.getValue() == null) {
                todoItem.unschedule();
            } else {
                todoItem.schedule(scheduled.getValue());
            }
            if (!project.getText().isEmpty()) {
                List<String> projects = new ArrayList<>(Arrays.asList(project.getText().split("\\s*,\\s*")));
                todoItem.setProjects(projects);
            }
            if (!context.getText().isEmpty()) {
                List<String> contexts = new ArrayList<>(Arrays.asList(context.getText().split("\\s*,\\s*")));
                todoItem.setContexts(contexts);
            }
            return todoItem;
        } else {
            return new TodoItem(
                    title.getText(),
                    details.getText(),
                    scheduled.getValue(),
                    new ArrayList<>(Arrays.asList(project.getText().split("\\s*,\\s*"))),
                    new ArrayList<>(Arrays.asList(context.getText().split("\\s*,\\s*")))
            );
        }
    }
}
