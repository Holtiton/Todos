package todos.GUI.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.MenuBar;
import todos.repository.ObservableTodoRepository;

public class MenuController {
    private ObservableTodoRepository repo;

    public MenuController(ObservableTodoRepository repo) {
        this.repo = repo;
    }

    @FXML
    private MenuBar menuBar;

    public void initialize() {
        Platform.runLater(() -> menuBar.setUseSystemMenuBar(true));
    }
}
