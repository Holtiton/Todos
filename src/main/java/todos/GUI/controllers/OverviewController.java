package todos.GUI.controllers;

import javafx.application.Platform;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyEvent;
import todos.GUI.EditDialog;
import todos.model.TodoItem;
import todos.repository.ObservableTodoRepository;

import java.util.Optional;

public class OverviewController {
    private final ObservableTodoRepository repo;

    public OverviewController(ObservableTodoRepository repo) {
        this.repo = repo;
    }

    @FXML
    private ListView<TodoItem> listView;

    public void initialize() {
        listView.setItems(repo.allTodosProperty());
        listView.setCellFactory(param -> new ListCell<TodoItem>() {
            final PseudoClass completedPseudoClass = PseudoClass.getPseudoClass("completed");
            final PseudoClass duePseudoClass = PseudoClass.getPseudoClass("due");
            final PseudoClass dueSoonPseudoClass = PseudoClass.getPseudoClass("dueSoon");

            @Override
            protected void updateItem(TodoItem item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                    pseudoClassStateChanged(completedPseudoClass, false);
                    pseudoClassStateChanged(duePseudoClass, false);
                    pseudoClassStateChanged(dueSoonPseudoClass, false);
                } else {
                    setText(item.toListViewString());
                    Integer dueInDays = item.dueInDays();
                    // TODO: This should be configurable
                    if (dueInDays != null && dueInDays >= 3 && dueInDays <= 5) {
                        pseudoClassStateChanged(dueSoonPseudoClass, true);
                    } else {
                        pseudoClassStateChanged(dueSoonPseudoClass, false);
                    }
                    if (dueInDays != null && dueInDays < 3) {
                        pseudoClassStateChanged(duePseudoClass, true);
                        pseudoClassStateChanged(dueSoonPseudoClass, false);
                    } else {
                        pseudoClassStateChanged(duePseudoClass, false);
                    }
                    if (item.isComplete()) {
                        pseudoClassStateChanged(completedPseudoClass, true);
                        pseudoClassStateChanged(duePseudoClass, false);
                        pseudoClassStateChanged(dueSoonPseudoClass, false);
                    } else {
                        pseudoClassStateChanged(completedPseudoClass, false);
                    }
                }
            }
        });
        listView.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                EditDialog dialog = new EditDialog(repo, listView.getSelectionModel().getSelectedItem());
                dialog.showAndWait();
                listView.refresh();
            }
        });

        Platform.runLater(this::setKeybinds);
    }

    private void setKeybinds() {
        Scene scene = listView.getScene();
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            switch (event.getCode()) {
                case X: {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Confirm deletion");
                    alert.setHeaderText("The selected Todos will be deleted.");
                    alert.setContentText("Are you ok with this?");

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.isPresent() && result.get() == ButtonType.OK) {
                        for (TodoItem item : listView.getSelectionModel().getSelectedItems()) {
                            repo.remove(item);
                        }
                    }  // ... user chose CANCEL or closed the dialog

                    break;
                }
                case C: {
                    for (TodoItem item : listView.getSelectionModel().getSelectedItems()) {
                        if (!item.isComplete()) {
                            item.complete();
                        } else {
                            item.reopen();
                        }
                    }
                    listView.refresh();
                    break;
                }
                case A: {
                    for (TodoItem item : listView.getSelectionModel().getSelectedItems()) {
                        item.archive();
                    }
                    listView.refresh();
                    break;
                }
            }
        });
    }
}
