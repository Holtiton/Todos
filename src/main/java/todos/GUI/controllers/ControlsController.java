package todos.GUI.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import todos.GUI.EditDialog;
import todos.model.TodoItem;
import todos.repository.ObservableTodoRepository;

import java.util.Optional;

public class ControlsController {
    private ObservableTodoRepository repo;

    public ControlsController(ObservableTodoRepository repo) {
        this.repo = repo;
    }

    @FXML
    private Button newTodoButton;

    @FXML
    private TextField quickTodo;

    public void initialize() {
        setActions();
        Platform.runLater(this::setKeybinds);
    }

    private void setActions() {
        newTodoButton.setOnAction(event -> {
            EditDialog dialog = new EditDialog(repo, null);
            Optional<TodoItem> newItem = dialog.showAndWait();
            newItem.ifPresent(todoItem -> repo.add(todoItem));
        });
        quickTodo.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                String todoText = quickTodo.getText();
                if (!todoText.isEmpty()) {
                    TodoItem todo = TodoItem.fromString(quickTodo.getText());
                    repo.add(todo);
                    quickTodo.clear();
                }
            }
        });
    }

    private void setKeybinds() {
        Scene scene = quickTodo.getScene();
        scene.addEventHandler(KeyEvent.KEY_RELEASED, event -> {
            if (event.getCode() == KeyCode.N) {
                quickTodo.requestFocus();
            }
            if (event.getCode() == KeyCode.M && !quickTodo.isFocused()) {
                newTodoButton.fire();
            }
        });
    }
}
