package todos.GUI;


import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import todos.GUI.controllers.EditTodoController;
import todos.model.TodoItem;
import todos.repository.ObservableTodoRepository;

import java.io.IOException;

public class EditDialog extends Dialog<TodoItem> {
    public EditDialog(ObservableTodoRepository repo, TodoItem todo) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/EditTodo.fxml"));
            Parent root = loader.load();
            EditTodoController controller = loader.getController();
            controller.setRepo(repo);
            controller.setTodo(todo);
            ButtonType okButton = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            getDialogPane().getButtonTypes().add(okButton);
            getDialogPane().getButtonTypes().add(cancelButton);
            getDialogPane().setContent(root);

            setResultConverter(buttonType -> {
                if (buttonType == ButtonType.OK) {
                    return controller.getTodoItem();
                } else {
                    return null;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
