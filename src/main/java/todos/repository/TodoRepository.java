package todos.repository;

import todos.model.TodoItem;

import java.util.List;

public interface TodoRepository {
    void add(TodoItem item);

    void remove(TodoItem item);

    List<TodoItem> allTodos();

}
