package todos.repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import todos.model.TodoItem;

import java.util.List;

/**
 * Wrapper class for <code>{@link InMemoryTodoRepository}</code> for JavaFX.
 * <p>
 * Observable wrapper for the repository which is POJO based as there
 * may come a need to develop other interfaces also.
 */
public class ObservableTodoRepositoryWrapper implements ObservableTodoRepository {
    private TodoRepository base;
    private ObservableList<TodoItem> todoItems;

    public ObservableTodoRepositoryWrapper(TodoRepository base) {
        this.base = base;
        this.todoItems = FXCollections.observableArrayList(base.allTodos());
    }

    @Override
    public void add(TodoItem item) {
        base.add(item);
        todoItems.add(item);
    }

    @Override
    public void remove(TodoItem item) {
        base.remove(item);
        todoItems.remove(item);
    }

    @Override
    public List<TodoItem> allTodos() {
        return base.allTodos();
    }

    @Override
    public ObservableList<TodoItem> allTodosProperty() {
        return todoItems;
    }
}
