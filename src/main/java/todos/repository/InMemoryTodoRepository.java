package todos.repository;

import todos.model.TodoItem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InMemoryTodoRepository implements TodoRepository {
    private List<TodoItem> todoItems = new ArrayList<>();

    public InMemoryTodoRepository() {
        LocalDate today = LocalDate.now();
        LocalDate fourDaysFromNow = today.plusDays(4);
        todoItems.add(new TodoItem("Todo 1", "something1", today, new ArrayList<>(), new ArrayList<>()));
        todoItems.add(new TodoItem("Todo 2", "something2", fourDaysFromNow, new ArrayList<>(), new ArrayList<>()));
        todoItems.add(new TodoItem("Todo 3", "something3", null, new ArrayList<>(), new ArrayList<>()));
        todoItems.add(new TodoItem("Todo 4", "something4", null, new ArrayList<>(), new ArrayList<>()));
        todoItems.add(new TodoItem("Todo 5", "something5", null, new ArrayList<>(), new ArrayList<>()));
    }

    @Override
    public void add(TodoItem item) {
        todoItems.add(item);
    }

    @Override
    public void remove(TodoItem item) {
        todoItems.remove(item);
    }

    @Override
    public List<TodoItem> allTodos() {
        return todoItems;
    }
}
