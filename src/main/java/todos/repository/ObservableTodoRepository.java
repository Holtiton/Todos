package todos.repository;

import javafx.collections.ObservableList;
import todos.model.TodoItem;

public interface ObservableTodoRepository extends TodoRepository {
    ObservableList<TodoItem> allTodosProperty();
}
