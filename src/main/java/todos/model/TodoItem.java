package todos.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.time.temporal.ChronoUnit.DAYS;

public class TodoItem {
    private String title;
    private String details;
    private LocalDate createdDate = LocalDate.now();
    private LocalDate completedDate;
    private LocalDate scheduledDate;
    private List<String> projects = new ArrayList<>();
    private List<String> contexts = new ArrayList<>();
    private TodoState currentState = TodoState.OPEN;

    final static private Pattern projectPattern = Pattern.compile("(?<!\\S)\\+(.*?)(?!\\S)");
    final static private Pattern contextPattern = Pattern.compile("(?<!\\S)@(.*?)(?!\\S)");

    private TodoItem() {

    }

    public static TodoItem fromString(String raw) {
        TodoItem newTodo = new TodoItem();
        StringJoiner sj = new StringJoiner(", ");
        Matcher projectMatcher = projectPattern.matcher(raw);
        while (projectMatcher.find()) {
            raw = raw.replace(projectMatcher.group(0), "");
            sj.add(projectMatcher.group(1));
        }
        newTodo.projects.addAll(Arrays.asList(sj.toString().split("\\s*,\\s*")));

        sj = new StringJoiner(", ");
        Matcher contextMatcher = contextPattern.matcher(raw);
        while (contextMatcher.find()) {
            raw = raw.replace(contextMatcher.group(0), "");
            sj.add(contextMatcher.group(1));
        }
        newTodo.contexts.addAll(Arrays.asList(sj.toString().split("\\s*,\\s*")));
        newTodo.title = raw.replaceAll(" +", " ").trim();

        // TODO: need to parse scheduled date also

        return newTodo;
    }

    public TodoItem(String title, String details,
                    LocalDate scheduledDate,
                    List<String> projects, List<String> contexts) {
        this.title = title;
        this.details = details;
        if (scheduledDate != null) {
            this.scheduledDate = scheduledDate;
            this.currentState = TodoState.SCHEDULED;
        }
        this.projects = projects;
        this.contexts = contexts;
    }

    public TodoItem(TodoItem old) {
        this.title = old.title;
        this.details = old.details;
        this.createdDate = old.createdDate;
        this.scheduledDate = old.scheduledDate;
        this.projects = old.projects;
        this.contexts = old.contexts;
        this.currentState = old.currentState;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (!title.isEmpty()) {
            this.title = title;
        }
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<String> getProjects() {
        return projects;
    }

    public void setProjects(List<String> projects) {
        this.projects = projects;
    }

    public List<String> getContexts() {
        return contexts;
    }

    public void setContexts(List<String> contexts) {
        this.contexts = contexts;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public LocalDate getScheduledDate() {
        return scheduledDate;
    }

    void setScheduledDate(LocalDate scheduledDate) {
        if (scheduledDate != null) {
            LocalDate today = LocalDate.now();
            if (scheduledDate.isBefore(today)) {
                throw new IllegalArgumentException("Cannot schedule todo in the past");
            } else {
                this.scheduledDate = scheduledDate;
            }
        } else {
            this.scheduledDate = null;
        }
    }

    public LocalDate getCompletedDate() {
        return completedDate;
    }

    void setCompletedDate(LocalDate completedDate) {
        this.completedDate = completedDate;
    }

    public TodoState getCurrentState() {
        return currentState;
    }

    public void complete() {
        this.currentState = this.currentState.complete(this);
    }

    public void reopen() {
        this.currentState = this.currentState.reopen(this);
    }

    public void archive() {
        this.currentState = this.currentState.archive(this);
    }

    public void postpone(int days) {
        this.currentState = this.currentState.postpone(this, days);
    }

    public void unschedule() {
        this.currentState = this.currentState.unschedule(this);
    }

    public void schedule(LocalDate scheduledDate) {
        this.currentState = this.currentState.schedule(this, scheduledDate);
    }

    public boolean isComplete() {
        return this.currentState == TodoState.COMPLETED;
    }

    public Integer dueInDays() {
        Integer daysDue;
        if (scheduledDate == null) {
            daysDue = null;
        } else {
            LocalDate today = LocalDate.now();
            daysDue = Math.toIntExact(DAYS.between(today, scheduledDate));

        }
        return daysDue;
    }

    public String toListViewString() {
        StringJoiner joiner = new StringJoiner(" ");
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (completedDate != null) {
            joiner.add(completedDate.format(dateFormat));
        }
        joiner.add(createdDate.format(dateFormat))
                .add(getTitle());
        if (scheduledDate != null) {
            joiner.add("scheduled:")
                    .add(scheduledDate.format(dateFormat));
        }
        return joiner.toString();
    }
}
