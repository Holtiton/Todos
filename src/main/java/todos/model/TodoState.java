package todos.model;

import java.time.LocalDate;

enum TodoState {
    OPEN {
        @Override
        TodoState schedule(TodoItem context, LocalDate date) {
            context.setScheduledDate(date);
            return SCHEDULED;
        }
        @Override
        TodoState complete(TodoItem context) {
            context.setCompletedDate(LocalDate.now());
            return COMPLETED;
        }
    },
    SCHEDULED {
        @Override
        TodoState schedule(TodoItem context, LocalDate date) {
            context.setScheduledDate(date);
            return SCHEDULED;
        }

        @Override
        TodoState unschedule(TodoItem context) {
            context.setScheduledDate(null);
            return OPEN;
        };

        @Override
        TodoState postpone(TodoItem context, int days) {
            final LocalDate currentDate = context.getScheduledDate();
            context.setScheduledDate(currentDate.plusDays(days));
            return SCHEDULED;
        }

        @Override
        TodoState complete(TodoItem context) {
            context.setCompletedDate(LocalDate.now());
            return COMPLETED;
        }
    },
    COMPLETED {
        @Override
        TodoState reopen(TodoItem context) {
            context.setCompletedDate(null);
            if (context.getScheduledDate() != null) {
                return SCHEDULED;
            }
            return OPEN;
        }

        @Override
        TodoState archive(TodoItem context) {
            return ARCHIVED;
        };
    },
    ARCHIVED {

    };

    TodoState schedule(TodoItem context, LocalDate date) {
        throw new IllegalStateException();
    };

    TodoState unschedule(TodoItem context) {
        throw new IllegalStateException();
    };

    TodoState postpone(TodoItem context, int days) {
        throw new IllegalStateException();
    };

    TodoState complete(TodoItem context) {
        throw new IllegalStateException();
    };

    TodoState reopen(TodoItem context) {
        throw new IllegalStateException();
    };

    TodoState archive(TodoItem context) {
        throw new IllegalStateException();
    };
}
